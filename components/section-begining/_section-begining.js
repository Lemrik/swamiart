$(document).ready(function() {
  // попап для видео
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});


	$(document).on("click", '.jq-selectbox__select-text' , function(event){
    event.preventDefault();
    $('.works__filter-select').toggleClass('js-select');
	});

});