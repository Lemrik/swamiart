$(function(){

  $("[data-style-slider]").slick({
		infinite: true,
		fade:false,
		dots:false,
		slidesToShow: 2,
		slidesToScroll: 2,
		pauseOnHover: false,
		autoplay: false,
		autoplaySpeed: 3000,
		arrows: true,
		responsive: [
			{
				breakpoint: 1259,
				settings: {
				 slidesToShow: 1,
				 slidesToScroll: 1
			 }}]
	});

});