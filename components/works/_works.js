$(function(){
  // скрипт для работы анимации стрелки в селекте
  $('.jq-selectbox__select-text').on("click", function(event){
    event.preventDefault();
    $('.works__filter-select').toggleClass('js-select');
  });

  // скрипт на обратную анимцию в селкте при клике в пустоту
  $(document).mouseup(function (e){ 
    event.preventDefault();
    var block = $(".jq-selectbox__select-text"); 
    if (!block.is(e.target) 
    && block.has(e.target).length === 0 ) {
      $('.works__filter-select').removeClass('js-select');
    }
  });

  // скрипт на покрас иконок, добавления в избранное и корзину
  $('.works__gallery-icon').on("click", function(){
    event.preventDefault();
    $(this).toggleClass('is-active');
  });

  // активация плагина для селекта 

  $('.works__filter-select').select2({
    minimumResultsForSearch: Infinity,
    containerCssClass: "works-custom-container",
    dropdownCssClass: "works-custom-dropdown"
  })
});