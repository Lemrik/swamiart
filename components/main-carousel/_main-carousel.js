// main-carousel
$(function(){
  // slider
  $("[data-main-carousel]").slick({
    infinite: true,
    fade:true,
    dots:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    pauseOnHover: false,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows:false
  });
});