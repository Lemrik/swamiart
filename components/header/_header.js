$(function(){
  // скрипт на открытие поиска
  $('.js-search').on('click', function(event){
    event.preventDefault();
    if($('.header-search').hasClass('js-active')){
      $('.header-search').removeClass('js-active').slideUp(400);
    }else{
      $('.header-search').slideDown(400).addClass('js-active');
    }
  });

  // скрипт для нормального сокрытия поиска
  $(document).click(function (e){ 

    if(!$(e.target).closest('.header-search , .js-search, .header-menu ').length){
      $('.header-search').slideUp(400).removeClass('js-active');
    }
  });

  // extra работа блока "ещё"
  $('.js-extra').on('click' , function(){
    event.preventDefault();
    if($('.header-extra').hasClass('is-active-extra')){
      $('.header-extra').removeClass('is-active-extra').slideUp(400);
      $('.js-extra').removeClass('js-triangle');
    }else{
      $('.header-extra').slideDown(400).addClass('is-active-extra');
      $('.js-extra').addClass('js-triangle');
    }
  });

  // скрипт для нормального сокрытия extra
  $(document).click(function (e){ 

    if(!$(e.target).closest('.header-extra , .js-extra').length){
      $('.header-extra').slideUp(400).removeClass('is-active-extra');
      $('.js-extra').removeClass('js-triangle');
    }
  });

  // header-settings открытие
  $('.js-user-select').on('click' , function(){
    event.preventDefault();
    if($('.header-settings').hasClass('is-active-user-select')){
      $('.header-settings').removeClass('is-active-user-select').slideUp(400);
      $('.js-user-select').removeClass('js-triangle');
    }else{
      $('.header-settings').slideDown(400).addClass('is-active-user-select');
      $('.js-user-select').addClass('js-triangle');
    }
  });

  // header-settings закрытие
  $(document).click(function (e){ 

    if(!$(e.target).closest('.header-settings , .js-user-select').length){
      $('.header-settings').slideUp(400).removeClass('is-active-user-select');
      $('.js-user-select').removeClass('js-triangle');
    }
  });

  // скрипт для бургера 
  $('.header-toggle').on('click', function(event){

    $('.header-toggle').toggleClass('is-active');

    if($('.header-menu').hasClass('js-active')){
      $('.header-menu').removeClass('js-active').slideUp(400);
    }else{
      $('.header-menu').slideDown(400).addClass('js-active');
    }
  });

  // клик вне блока бургера
  $(document).click(function (e){ 

    if(!$(e.target).closest('.header-menu__item , .header-toggle').length){
      $('.header-menu').slideUp(400).removeClass('js-active');
      $('.header-toggle').removeClass('is-active');
    }
  });

  // попапы
  $('.popup-entry').magnificPopup({
		type: 'inline',
		preloader: false,
		// focus: '#name',

		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 768) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

  // селект для смены язков
  $('.header-tools__select').select2({
    minimumResultsForSearch: Infinity,
    dropdownCssClass: "header-custom-dropdown"
  })

   // плагин валидации
   $.extend( $.validator.messages, {
    required: "Это поле необходимо заполнить.",
    remote: "Пожалуйста, введите правильное значение.",
    email: "Пожалуйста, введите корректный email.",
    url: "Пожалуйста, введите корректный URL.",
    date: "Пожалуйста, введите корректную дату.",
    dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
    number: "Пожалуйста, введите число.",
    digits: "Пожалуйста, вводите только цифры.",
    creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
    equalTo: "Пожалуйста, введите такое же значение ещё раз.",
    extension: "Пожалуйста, выберите файл с правильным расширением.",
    maxlength: $.validator.format( "Пожалуйста, введите не больше {0} символов." ),
    minlength: $.validator.format( "Пожалуйста, введите не меньше {0} символов." ),
    rangelength: $.validator.format( "Пожалуйста, введите значение длиной от {0} до {1} символов." ),
    range: $.validator.format( "Пожалуйста, введите число от {0} до {1}." ),
    max: $.validator.format( "Пожалуйста, введите число, меньшее или равное {0}." ),
    min: $.validator.format( "Пожалуйста, введите число, большее или равное {0}." )
  } );
  
  // валидация форм в попапе
  $('.popup-form-registration').validate({
      errorPlacement: function(error, element) {
          var $parent = element.parent();
          var $title = element.siblings('.popup-register__input-title');
          $parent.append(error);
          $title.addClass('error-title')
      },
      submitHandler: function(form) {
              $(form).trigger("formSubmit");
      },

      rules: {
        ignore:".popup-register__input-title",

        name: {
          required: true
        },

        surname: {
          required: true
        },

        email: {
          required: true
        },

        password: {
          required: true,
          minlength: 8
        },

        agreement:{
          required: true
        }
      },

      messages: {
        name: {
          required: "Введите имя"
        },

        surname: {
          required: "Введите фамилию"
        },

        password: {
          required: "Это поле необходимо заполнить",
          minlength: jQuery.validator.format("Пожалуйста, введите не меньше 8 символов")
        }
      },

      highlight: function(element, errorClass, validClass) {
        $(element).siblings('.popup-register__input-title').addClass('error-title');
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).siblings('.popup-register__input-title').removeClass('error-title');
      }
  });

  $('.popup-form-authorization').validate({
    errorPlacement: function(error, element) {
        var $parent = element.parent();
        var $title = element.siblings('.popup-register__input-title');
        $parent.append(error);
        $title.addClass('error-title')
    },
    submitHandler: function(form) {
            $(form).trigger("formSubmit");
    },

    rules: {
      ignore:".popup-register__input-title",

      email: {
        required: true
      },

      password: {
        required: true,
        minlength: 8
      },
    },

    messages: {
      password: {
        required: "Это поле необходимо заполнить",
        minlength: jQuery.validator.format("Пожалуйста, введите не меньше 8 символов")
      }
    },

    highlight: function(element, errorClass, validClass) {
      $(element).siblings('.popup-register__input-title').addClass('error-title');
    },
    unhighlight: function(element, errorClass, validClass) {
      $(element).siblings('.popup-register__input-title').removeClass('error-title');
    }
  });

  $('.popup-form-remove-pass').validate({
    errorPlacement: function(error, element) {
        var $parent = element.parent();
        var $title = element.siblings('.popup-register__input-title');
        $parent.append(error);
        $title.addClass('error-title')
    },
    submitHandler: function(form) {
            $(form).trigger("formSubmit");
            $('.popup-remove-pass').addClass('hide');
            $('.popup-successful').removeClass('hide');
    },

    rules: {
      ignore:".popup-register__input-title",
      email: {
        required: true
      }
    },

    highlight: function(element, errorClass, validClass) {
      $(element).siblings('.popup-register__input-title').addClass('error-title');
    },
    unhighlight: function(element, errorClass, validClass) {
      $(element).siblings('.popup-register__input-title').removeClass('error-title');
    }
  });

  // регулярка для имени и фамилии
  $('.popup-register__name , .popup-register__surname').on('input', function() {
    var value = $(this).val();
    value = value.replace(/[^a-zA-Zа-яА-ЯЁё\s\-]/ig, '');
    $(this).val(value);
  });

  // закрытие попапов
  $(document).on('click', '.mfp-close' , function(){
    $('.popup-authorization').addClass('hide');
    $('.popup-remove-pass').addClass('hide');
    $('.popup-successful').addClass('hide');
    $('.popup-registration').removeClass('hide');
  });

  // вид попапа авторизации
  $(document).on('click', '.js-log-in' , function(){
    $('.popup-registration').addClass('hide');
    $('.popup-remove-pass').addClass('hide');
    $('.popup-authorization').removeClass('hide');
  });

  // // возвращение к главному попапу от авторизации
  $(document).on('click', '.js-reg-in' , function(){
    $('.popup-authorization').addClass('hide');
    $('.popup-registration').removeClass('hide');
  });

  // // попап восстановления пароля
  $(document).on('click', '.js-remove-pas' , function(){
    $('.popup-authorization').addClass('hide');
    $('.popup-remove-pass').removeClass('hide');
  });

});

